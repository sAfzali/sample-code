package com.sample.afzali.samplecode;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.sample.afzali.samplecode.utils.BaseActivity;
import com.sample.afzali.samplecode.utils.PublicMethods;

public class SharedPrefcActivity extends BaseActivity implements View.OnClickListener {
    EditText userName, mobile;
    String toastValue = "Data Has been saved!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_prefc);
        bind();
        loadData();
    }


    protected void bind() {
        userName = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        findViewById(R.id.save).setOnClickListener(this);
    }

    protected void loadData() {
/*
String defNameValu = PreferenceManager.getDefaultSharedPreferences(this)
.getString("user_name", "");
String defMobileValue = PreferenceManager.getDefaultSharedPreferences(this)
.getString("mobile", "");
*/
        String defNameValue = PublicMethods.getData("user_name", "");
        String defMobileValue = PublicMethods.getData("mobile", "");
        if (defNameValue.length() > 0) {
            userName.setText(defNameValue);
        }
        if (defMobileValue.length() > 0) {
            mobile.setText(defMobileValue);
        }

    }

    @Override
    public void onClick(View v) {
        String userValue = userName.getText().toString();
        String mobileValue = mobile.getText().toString();
/*
PreferenceManager.getDefaultSharedPreferences(this)
.edit().putString("user_name", userValue).apply();
PreferenceManager.getDefaultSharedPreferences(this)
.edit().putString("mobile", mobileValue).apply();
*/
        PublicMethods.saveData("user_name", userValue);
        PublicMethods.saveData("mobile", mobileValue);


        userName.setText("");
        mobile.setText("");
        PublicMethods.toast(this, toastValue);


    }
}
