package com.sample.afzali.samplecode.listview;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sample.afzali.samplecode.R;
import com.sample.afzali.samplecode.utils.BaseActivity;
import com.sample.afzali.samplecode.utils.PublicMethods;

public class MyListActivity extends BaseActivity {
    ListView myList;
    String names[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list);
        bind();

        String names[] = {
                "Alireza",
                "Amir",
                "Mohammad",
                "Soheil",
                "Hossein",
                "Reza",
                "AmirHossein",
                "Maryam",
                "Niloofar",
        };
        UsersListAdapter adapter = new UsersListAdapter(mContext, names);
        myList.setAdapter(adapter);
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PublicMethods.toast(mContext, "Item number " + position);
            }
        });

    }

    void bind() {
        myList = findViewById(R.id.my_list);
    }


}
