package com.sample.afzali.samplecode.foodslist;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sample.afzali.samplecode.R;
import com.sample.afzali.samplecode.utils.PublicMethods;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FoodsListActivity extends AppCompatActivity {
    ListView foodsListView;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foods_list);
        foodsListView = findViewById(R.id.foods_list_view);
        FoodModel pizza = new FoodModel();
        pizza.setName("pizza");
        pizza.setPrice(25000);
        pizza.setImage("https://www.cicis.com/media/1243/pizza_adven_zestypepperoni.png");
        FoodModel fesenjoon = new FoodModel();
        fesenjoon.setName("fesenjoon");
        fesenjoon.setPrice(20000);
        fesenjoon.setImage("https://www.cicis.com/media/1243/pizza_adven_zestypepperoni.png");

        List<FoodModel> foods = new ArrayList<>();
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(pizza);
        FoodsAdapter adapter = new FoodsAdapter(this, foods);
        foodsListView.setAdapter(adapter);
        foodsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PublicMethods.toast(mContext, "Item is :" + position);
            }
        });

    }


}
