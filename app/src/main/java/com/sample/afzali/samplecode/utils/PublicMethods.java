package com.sample.afzali.samplecode.utils;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class PublicMethods {


    public static void toast(Context mcontext, String msg) {
        Toast.makeText(mcontext, msg, Toast.LENGTH_SHORT).show();
    }

    public static void saveData(String key, String value) {
        Hawk.put(key, value);
/*
PreferenceManager.getDefaultSharedPreferences(mContext)
.edit().putString(key, value).apply();
*/
    }

    public static String getData(String key, String value) {
        return Hawk.get(key, value);
/*
return PreferenceManager.getDefaultSharedPreferences(mContext)
.getString(key, value);
*/
    }
}
