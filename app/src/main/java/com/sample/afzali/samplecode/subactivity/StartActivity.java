package com.sample.afzali.samplecode.subactivity;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.sample.afzali.samplecode.utils.Cons;
import com.sample.afzali.samplecode.utils.PublicMethods;
import com.sample.afzali.samplecode.R;

public class StartActivity extends AppCompatActivity {
    TextView result;
    int subRequestCode = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        bind();

    }

    protected void bind() {
        result = findViewById(R.id.result);
        findViewById(R.id.get_data).setOnClickListener(V -> {
            Intent subIntent = new Intent(this, SubActivity.class);
            startActivityForResult(subIntent, subRequestCode);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == subRequestCode) {
            if (resultCode == Activity.RESULT_OK)
                result.setText(data.getStringExtra(Cons.VALUE));
            else
                PublicMethods.toast(this, getString(R.string.data_is_empty));
        }
    }


}
