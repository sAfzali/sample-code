package com.sample.afzali.samplecode;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class SubActivitySample extends AppCompatActivity  {
    ImageView imageView;
    Button openCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_sample);
        bind();
    }

    void bind() {

        imageView = findViewById(R.id.image_camera);
        findViewById(R.id.openCamera).setOnClickListener(V -> {
            EasyImage.openCamera(this, 0);
        });
//        findViewById(R.id.openCamera).setOnClickListener(this);
// ->>>>>   implements View.OnClickListener


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                Glide.with(SubActivitySample.this).load(imageFile).into(imageView);
            }
        });
    }

//    @Override
//    public void onClick(View v) {
//
//
//    }
}