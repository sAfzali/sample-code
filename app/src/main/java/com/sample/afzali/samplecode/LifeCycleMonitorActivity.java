package com.sample.afzali.samplecode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class LifeCycleMonitorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle);
        Log.d("monitor_tag", "onCreate: ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("monitor_tag", "onStart: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("monitor_tag", "onResume: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("monitor_tag", "onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("monitor_tag", "onStop: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("monitor_tag", "onDestroy: ");
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}

