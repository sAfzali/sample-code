package com.sample.afzali.samplecode.subactivity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.sample.afzali.samplecode.R;
import com.sample.afzali.samplecode.utils.Cons;

public class SubActivity extends AppCompatActivity implements View.OnClickListener {
    EditText textDetailName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        bind();
    }


    void bind() {
        textDetailName = findViewById(R.id.text_view_name);
        findViewById(R.id.set_data).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String name = textDetailName.getText().toString().trim();
        Intent callBackIntent = new Intent();

        callBackIntent.putExtra(Cons.VALUE, name);
        if (name.length() > 0) {
            setResult(Activity.RESULT_OK, callBackIntent);

        } else
            setResult(Activity.RESULT_CANCELED, callBackIntent);
        finish();
    }
}
